const { expect } = require('@jest/globals');
const yahtzee = require('./yahtzee');

test("Si nous obtenons la suite : 1, 1, 2, 2, 4. On veut pour le combo '1' : 2 répétitions et 2 points ET pour le combo '2' : 2 répétitions et 4 points", () => {
    expect(yahtzee([1, 1, 2, 2, 4])).toEqual([
        { NomCombo: '1', NbrCombo: 2, Pountos: 2 },
        { NomCombo: '2', NbrCombo: 2, Pountos: 4 },
        { NomCombo: '3', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '4', NbrCombo: 1, Pountos: 4 },
        { NomCombo: '5', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FourOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FullHouse', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
      ]);
})
//suivant la même logique on a simplifié la description

test("Si nous obtenons la suite : 6, 1, 2, 5, 4. Combo '1' : 1 rép 1 point / Combo '2' : 2 réps 4 points / Combo '4' : 1 rép 4 points / Combo '5' : 1 rép 5 points / Combo '6' : 1 rép 6 points", () => {
    expect(yahtzee([6, 1, 2, 5, 4])).toEqual([
        { NomCombo: '1', NbrCombo: 1, Pountos: 1 },
        { NomCombo: '2', NbrCombo: 1, Pountos: 2 },
        { NomCombo: '3', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '4', NbrCombo: 1, Pountos: 4 },
        { NomCombo: '5', NbrCombo: 1, Pountos: 5 },
        { NomCombo: '6', NbrCombo: 1, Pountos: 6 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FourOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FullHouse', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
    ]);
})

// Il s'avère que ce test doit maintenant afficher 'ThreeOfKind' 1 fois pour le score correspondant à l'ensemble des dés  
test("Si nous obtenons la suite : 4, 4, 2, 2, 4. Combo '2' : 2 réps 4 points /  Combo '4' : 3 rép 12 points / Combo 'ThreeOfKind' : 1 rép 16 points", () => {
    expect(yahtzee([4, 4, 2, 2, 4])).toEqual([
        { NomCombo: '1', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '2', NbrCombo: 2, Pountos: 4 },
        { NomCombo: '3', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '4', NbrCombo: 3, Pountos: 12 },
        { NomCombo: '5', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 1, Pountos: 16 },
        { NomCombo: 'FourOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FullHouse', NbrCombo: 1, Pountos: 25 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
    ]);
})

// Il s'avère que ce test doit maintenant afficher 'FourOfKind' 1 fois pour le score correspondant à l'ensemble des dés  
test("Si nous obtenons la suite : 4, 4, 4, 2, 4. Combo '2' : 1 réps 2 points /  Combo '4' : 4 rép 16 points / Combo 'ThreeOfKind' : 1 rép 18 points / Combo 'FourOfKind' : 1 rép 18 points", () => {
    expect(yahtzee([4, 4, 4, 2, 4])).toEqual([
        { NomCombo: '1', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '2', NbrCombo: 1, Pountos: 2 },
        { NomCombo: '3', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '4', NbrCombo: 4, Pountos: 16 },
        { NomCombo: '5', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 1, Pountos: 18 },
        { NomCombo: 'FourOfKind', NbrCombo: 1, Pountos: 18 },
        { NomCombo: 'FullHouse', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
    ]);
})

// Il s'avère que ce test doit maintenant afficher 'FullHouse' 1 fois pour le score correspondant à 25  
test("Si nous obtenons la suite : 4, 4, 4, 2, 2. Combo '2' : 2 réps 4 points /  Combo '4' : 3 rép 12 points / Combo 'ThreeOfKind' : 1 rép 2 points / Combo 'FullHouse' : 1 rép 25 points", () => {
    expect(yahtzee([4, 4, 4, 2, 2])).toEqual([
        { NomCombo: '1', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '2', NbrCombo: 2, Pountos: 4 },
        { NomCombo: '3', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '4', NbrCombo: 3, Pountos: 12 },
        { NomCombo: '5', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 1, Pountos: 16 },
        { NomCombo: 'FourOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FullHouse', NbrCombo: 1, Pountos: 25 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
    ]);
})

// Il s'avère que ce test doit maintenant afficher 'Yahtzee' 1 fois pour le score correspondant à 50 
test("Si nous obtenons la suite : 4, 4, 4, 4, 4. Combo '4' : 4 rép 16 points / Combo 'Yahtzee' : 1 rép 50 points", () => {
    expect(yahtzee([4, 4, 4, 4, 4])).toEqual([
        { NomCombo: '1', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '2', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '3', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '4', NbrCombo: 5, Pountos: 20 },
        { NomCombo: '5', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 1, Pountos: 20 },
        { NomCombo: 'FourOfKind', NbrCombo: 1, Pountos: 20 },
        { NomCombo: 'FullHouse', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'Yahtzee', NbrCombo: 1, Pountos: 50 },
        { NomCombo: 'SmallStraight', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
    ]);
}) 

// Il s'avère que ce test doit maintenant afficher 'SmallStraight' 1 fois pour le score correspondant à "à" 
test("Si nous obtenons la suite : 1,2,3,4,2 le Combo 'SmallStraight' : 1 rép 30 points", () => {
    expect(yahtzee([1,2,3,4,2])).toEqual([
        { NomCombo: '1', NbrCombo: 1, Pountos: 1 },
        { NomCombo: '2', NbrCombo: 2, Pountos: 4 },
        { NomCombo: '3', NbrCombo: 1, Pountos: 3 },
        { NomCombo: '4', NbrCombo: 1, Pountos: 4 },
        { NomCombo: '5', NbrCombo: 0, Pountos: 0 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FourOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FullHouse', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 1, Pountos: 30 },
        { NomCombo: 'LargeStraight', NbrCombo: 0, Pountos: 0 }
    ]);
}) 

// Il s'avère que ce test doit maintenant afficher 'LargeStraight' 1 fois pour le score correspondant à "à" 
test("Si nous obtenons la suite : 1,2,3,4,5 le Combo 'LargeStraight' : 1 rép 40 points", () => {
    expect(yahtzee([1,2,3,4,5])).toEqual([
        { NomCombo: '1', NbrCombo: 1, Pountos: 1 },
        { NomCombo: '2', NbrCombo: 1, Pountos: 2 },
        { NomCombo: '3', NbrCombo: 1, Pountos: 3 },
        { NomCombo: '4', NbrCombo: 1, Pountos: 4 },
        { NomCombo: '5', NbrCombo: 1, Pountos: 5 },
        { NomCombo: '6', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'ThreeOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FourOfKind', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'FullHouse', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'Yahtzee', NbrCombo: 0, Pountos: 0 },
        { NomCombo: 'SmallStraight', NbrCombo: 1, Pountos: 30 },
        { NomCombo: 'LargeStraight', NbrCombo: 1, Pountos: 40 }
    ]);
})