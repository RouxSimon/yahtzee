const dice = [1,2,3,4,5];

function yahtzee(dice){
    let onesRule=0;
    let TwosRule=0;
    let ThreesRule=0;
    let FoursRule=0;
    let FivesRule=0;
    let SixesRule=0;
    let TwoOfKind = 0;
    let ThreeOfKind = 0;
    let FourOfKind = 0;
    let FullHouse = 0;
    let Yahtzee = 0;
    let SmallStraight = 0;
    let LargeStraight = 0;

    const counts = {};
    dice.forEach(function (x) {
        counts[x] = (counts[x] || 0) + 1;

        if (counts[x]==2) {
            TwoOfKind = TwoOfKind+1;
        }
        if (counts[x]==3) {
            ThreeOfKind = 1;
        }
        if (counts[x]==4) {
            FourOfKind = 1;
        }
        if (counts[x]==5) {
            Yahtzee = 1;
        }
        if(
            (dice.includes(1) && dice.includes(2) && dice.includes(3) && dice.includes(4))
            ||
            (dice.includes(2) && dice.includes(3) && dice.includes(4) && dice.includes(5))
            ||
            (dice.includes(3) && dice.includes(4) && dice.includes(5) && dice.includes(6))
        ) {
            SmallStraight = 1
        }
        if(
            (dice.includes(1) && dice.includes(2) && dice.includes(3) && dice.includes(4) && dice.includes(5))
            ||
            (dice.includes(2) && dice.includes(3) && dice.includes(4) && dice.includes(5) && dice.includes(6))
        ) {
            LargeStraight = 1
        }

        
        // console.log('ThreeOfKind: ' + ThreeOfKind + ' TwoOfKind: ' + TwoOfKind);
        
        if (ThreeOfKind==1 && TwoOfKind==2) {
            FullHouse = 1;
        }
    });

    var res = new Array();

    for (let i = 0; i < dice.length; i++) {
        if (dice[i]===1) {
            onesRule++;
        }  else if (dice[i]===2){
            TwosRule++;
        }  else if (dice[i]===3){
            ThreesRule++;
        }  else if (dice[i]===4){
            FoursRule++;
        }  else if (dice[i]===5){
            FivesRule++;
        }  else if (dice[i]===6){
            SixesRule++;
        }
    }

    PountosTOK = 0;
    PountosFOK = 0;
    PountosFH = 0;
    PountosYahtzee = 0;
    PountosLargeStraight = 0;
    PountosSmallStraight = 0
    
    if (ThreeOfKind==1){
        PountosTOK = dice.reduce((a, b)=> a + b,0);
    } 
    if (FourOfKind==1){
        PountosFOK = dice.reduce((a, b)=> a + b,0);
    }
    if (FullHouse==1){
        PountosFH = 25;
    }
    if (Yahtzee==1){
        PountosYahtzee = 50;
    } 
    if(SmallStraight==1) {
        PountosSmallStraight = 30
    }
    if(LargeStraight==1) {
        PountosLargeStraight = 40
    }

    // res = [Nom de la figure, Nombre de fois que la figure se reproduit, Nombre de point)]
    let ComboUn = {NomCombo:'1',NbrCombo:onesRule,Pountos:1*onesRule};
    let ComboDeux = {NomCombo:'2',NbrCombo:TwosRule,Pountos:2*TwosRule};
    let ComboTrois = {NomCombo:'3',NbrCombo:ThreesRule,Pountos:3*ThreesRule};
    let ComboQuarte = {NomCombo:'4',NbrCombo:FoursRule,Pountos:4*FoursRule};
    let ComboCinq = {NomCombo:'5',NbrCombo:FivesRule,Pountos:5*FivesRule};
    let ComboSix = {NomCombo:'6',NbrCombo:SixesRule,Pountos:6*SixesRule};
    let ComboTOK = {NomCombo:'ThreeOfKind',NbrCombo:ThreeOfKind,Pountos:PountosTOK};
    let ComboFOK = {NomCombo:'FourOfKind',NbrCombo:FourOfKind,Pountos:PountosFOK};
    let ComboFH = {NomCombo:'FullHouse',NbrCombo:FullHouse,Pountos:PountosFH};
    let ComboYahtzee = {NomCombo:'Yahtzee',NbrCombo:Yahtzee,Pountos:PountosYahtzee};
    let ComboSS = {NomCombo:'SmallStraight',NbrCombo:SmallStraight,Pountos:PountosSmallStraight};
    let ComboLS = {NomCombo:'LargeStraight',NbrCombo:LargeStraight,Pountos:PountosLargeStraight};

    res.push(ComboUn);
    res.push(ComboDeux);
    res.push(ComboTrois);
    res.push(ComboQuarte);
    res.push(ComboCinq);
    res.push(ComboSix);
    res.push(ComboTOK);
    res.push(ComboFOK);
    res.push(ComboFH);
    res.push(ComboYahtzee);
    res.push(ComboSS);
    res.push(ComboLS);
    
    console.log(res)
    // console.log(dice)

    return res
};

yahtzee(dice);


module.exports = yahtzee;